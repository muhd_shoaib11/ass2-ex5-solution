﻿using System;
using System.Data;
using System.Data.SqlClient;

class Ex5 {
    static void Main(string[] args)
    {
        string server = args[0];
        string instance = args[1];

        Ex5 ex5 = new Ex5();
        SqlConnection conn = ex5.DBconnection(server, instance);

        double ReaderLoadTime = ex5.MeasureExecutionTime(conn, 75000, ex5.SqlDataReaderMethod);
        double AdapterFillTime = ex5.MeasureExecutionTime(conn, 75000, ex5.SqlDataAdapterMethod);
        Console.WriteLine("SqlDataReader load speed 75000 rows: " + ReaderLoadTime);
        Console.WriteLine("SqlDataAdapter fill speed 75000 rows: " + AdapterFillTime);

        ReaderLoadTime = ex5.MeasureExecutionTime(conn, 50000, ex5.SqlDataReaderMethod);
        AdapterFillTime = ex5.MeasureExecutionTime(conn, 50000, ex5.SqlDataAdapterMethod);
        Console.WriteLine("SqlDataReader load speed 50000 rows: " + ReaderLoadTime);
        Console.WriteLine("SqlDataAdapter fill speed 50000 rows: " + AdapterFillTime);

        ReaderLoadTime = ex5.MeasureExecutionTime(conn, 25000, ex5.SqlDataReaderMethod);
        AdapterFillTime = ex5.MeasureExecutionTime(conn, 25000, ex5.SqlDataAdapterMethod);
        Console.WriteLine("SqlDataReader load speed 25000 rows: " + ReaderLoadTime);
        Console.WriteLine("SqlDataAdapter fill speed 25000 rows: " + AdapterFillTime);

        ReaderLoadTime = ex5.MeasureExecutionTime(conn, 1000, ex5.SqlDataReaderMethod);
        AdapterFillTime = ex5.MeasureExecutionTime(conn, 1000, ex5.SqlDataAdapterMethod);
        Console.WriteLine("SqlDataReader load speed 1000 rows: " + ReaderLoadTime);
        Console.WriteLine("SqlDataAdapter fill speed 1000 rows: " + AdapterFillTime);

        Console.ReadKey();
    }

    private double MeasureExecutionTime(SqlConnection conn, int numRows, Action<SqlConnection, int> Method)
    {
        double time = 0; ;
        for (int i = 0; i < 10; i++)
        {
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            Method(conn, numRows);
            watch.Stop();
            if (i == 5 || i == 6)
            {
                time += watch.ElapsedMilliseconds;
            }
        }
        return time/2;
    }

    private SqlConnection DBconnection(string server, string instance)
    {
        string database = "WatsonAnalyticsSampleDatabase";
        string connString = "Integrated Security=true" + ";" + "database=" + database + ";" + "server=" + server + "\\" + instance;
        SqlConnection conn = new SqlConnection(connString);
        return conn;
    }

    private void SqlDataReaderMethod(SqlConnection conn, int numRows)
    {
        conn.Open();
        String query = "SELECT top " + numRows + " * FROM SampleTable;";
        SqlCommand command = new SqlCommand(query, conn);
        SqlDataReader reader = command.ExecuteReader();
        DataTable dataTable = new DataTable();
        dataTable.Load(reader);
        conn.Close();
    }

    private void SqlDataAdapterMethod(SqlConnection conn, int numRows)
    {
        conn.Open();
        String query = "SELECT top " + numRows + " * FROM SampleTable;";
        SqlCommand command = new SqlCommand(query, conn);
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dataTable = new DataTable();
        adapter.Fill(dataTable);
        conn.Close();
    }
}